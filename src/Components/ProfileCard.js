import React from 'react';
import styled from 'styled-components';
import BgTop from '../images/bg-pattern-top.svg';
import BgBottom from '../images/bg-pattern-bottom.svg';

//carta
const Card = styled.div`

  width: 100%;
  height: 39.4rem;
  max-width: 35rem;
  overflow: hidden;
  position: relative;
  text-align: left;
  padding-left: 40px;
  border-radius: 1.5rem;
  margin: 0 auto;
  background-color:#fff;
  box-shadow: 0px 20px 20px rgba(8,70,94,0.50);
  transition: transform 0.2s ease-out;
  backface-visibility: hidden;
  -webkit-backface-visibility: hidden;
  transition: .8s ease;

  &:hover
  {
    transform: scale(1.1);
    box-shadow: 0px 0px 20px rgba(220,220,220,0.50);
    animation-duration: 3s;   
  }

`;
//contenedor parte que rota
const Informacioncontainer = styled.section`
position relative;
width: 300px;
height: 400px;
display: flex;
flex-direction: column;
transform-style:preserve-3d;
transition: .8s ease;
&:hover
{
    transform: rotateY(180deg);
}
`;
//lado A parte que rota
const Info1 = styled.div`
width: 100%;
height: 100%;
position: absolute;
top:0;
left: 0;
margin-top: 2.5rem;
padding-right: 5.2rem;
backface-visibility: hidden;
-webkit-backface-visibility: hidden;
@media (min-width: 480px) {
  padding-right: 5.4rem;
}
`
//lado B de la parte que rota
const Infotwo = styled.div`
width: 100%;
height: 100%;
flex-direction: column;
position: absolute;
top:0;
left: 0;
margin-top: 2.5rem;
padding-right: 5.2rem;
transform: rotateY(180deg);
backface-visibility: hidden;
-webkit-backface-visibility: hidden;
@media (min-width: 480px) {
  padding-right: 5.4rem;
}
`
//linea divisora
const Linea = styled.section`
    width: 90%;
    height: .1rem;
    background-color: #ADACAC;
`
//contenedor cargo
const ImagenPcontainer= styled.div`

`
const ImagenP = styled.div`
    top: 0;
    left: 0;
    width: 100%;
    height: 14rem;
    position: absolute;
    box-shadow: 0px 0px 50px rgba(8,70,94,0.50);
`;
//Avatar personal
const Avatar = styled.div`
  z-index: 1;
  border-radius: 50%;
  margin-top: 8.7rem;
  margin-bottom: 1rem;
  position: relative;
  align-items: center;
  display: inline-flex;
  box-shadow: 10px 0px 20px rgba(8,70,94,0.50);;
  img 
  {
    width: 9.6rem;
    height: 9.6rem;
    border-radius: 50%;
    border: .5rem solid #fff;
    @media (min-width: 480px) 
    {
      width: 10.6rem;
      height: 10.6rem;
    }
  }
`;

/* Comoponente para los textos */
const TextComponent = styled.p`
    
    font-weight: ${props => props.fontWeight};
    line-height: ${props => props.lineHeight};
    font-size: ${props => props.fontSize};
    color: ${props => props.color};
    margin:0;
    
`
const Text = (props)=>{
  return(
    <TextComponent 
      fontWeight={props.fontWeight}
      lineHeight={props.lineHeight}
      fontSize={props.fontSize}
      color={props.color}>
      {props.text}
    </TextComponent>
  )
}
// Container
const ProfileContainerComponent = styled.section`
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
  display: grid;
  grid-template-columns:repeat(3, 1fr) ;
  grid-template-rows:repeat(4, 1fr) ;
  grid-row-gap: 5rem;
  grid-column-gap: 3rem;
  margin: 0;
  column-gap: 1em;
  width: 1350px;
  padding: 14.7rem 2.4rem 14.6rem 2.5rem;
  background-color: #19A2AE;
  background-image: url(${BgTop}), url(${BgBottom});
  background-position: -76rem -48rem, 15rem 27rem;
  @media (min-width: 768px) 
  {
    background-position: -28.57rem -50.79rem, bottom -59.2rem right -37.6rem;
  }
`
;
const ProfileContainer = (props)=>{
  return(
    <ProfileContainerComponent>
      {props.children}
    </ProfileContainerComponent>
  )
}

export const ProfileCard = (props)=> {
  return (
      /* agregar container */
      <ProfileContainer> 
          {
          props.userInfo.map((user,index)=>
          (
            <Card key={index}>
              <ImagenPcontainer>
                <ImagenP>
                  <img src={user.personal} alt="Imagen personal"/>
                </ImagenP>
              </ImagenPcontainer>
                <Avatar>
                  <img src={user.avatar} alt="Avatars"/>
                </Avatar>
              <Text text={user.username} fontSize='2.1rem' color={user.colorName} lineHeight='2.3rem' fontWeight='700'/>
              <Text text={user.email} fontSize='1.6rem' color={user.colorEmail} fontWeight='400' lineHeight='2.85rem'/>
              <Linea></Linea>
              <Informacioncontainer>
                <Info1>
                  <Text text={user.informacion} fontSize='1.8rem' color={user.colorName} lineHeight='1.8rem' fontWeight='700'/>
                  <Text text={user.informacion2} fontSize='1.6rem' color={user.colorEmail} fontWeight='400'/>
                </Info1>
                <Infotwo>
                  <Text text={user.seginformacion} fontSize='1.8rem' color={user.colorinfo} lineHeight='1.8rem' fontWeight='700'/>
                  <Text text={user.seginformacion2} fontSize='1.6rem' color={user.colorinfo2} fontWeight='400'/>
                </Infotwo>
              </Informacioncontainer>
            </Card>
            )
          )
        }
      </ProfileContainer>
  );
}
