import GlobalStyles from './GlobalStyles';
import {ProfileCard} from './Components/ProfileCard';

const users = [
  {
    id: 1,
    personal: '/assets/images/personal.svg',
    username: 'Carlos Hernán López Ruiz',
    colorName:'#2D3248',
    email: 'clopez@pedagogica.edu.co',
    colorEmail: '#969696',
    informacion: 'Información',
    informacion2: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    avatar: '/assets/images/Avatar4.png',
    seginformacion: 'Información ',
    seginformacion2: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    colorinfo:'#19A2AE'
  },
  {
    id: 2,
    personal: '/assets/images/personal2.svg',
    username: 'Lígia Lozano Cifuentes',
    colorName:'#2D3248',
    email: 'lclozano@pedagogica.edu.co',
    colorEmail: '#969696',
    informacion: 'Información',
    informacion2: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    avatar: '/assets/images/Avatar6.png',
    seginformacion: 'Información ',
    seginformacion2: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    colorinfo:'#19A2AE'
  },
  {
    id: 3,
    personal: '/assets/images/personal3.svg',
    username: 'Jorge Leonardo Hernández Rozo',
    colorName:'#2D3248',
    email: 'jlhernandez@pedagogica.edu.co',
    colorEmail: '#969696',
    informacion: 'Información',
    informacion2: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    avatar: '/assets/images/Avatar3.png',
    seginformacion: 'Información ',
    seginformacion2: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    colorinfo:'#19A2AE'
  },
  {
    id: 4,
    personal: '/assets/images/personal4.svg',
    username: 'Johann Mateo Soler López',
    colorName:'#2D3248',
    email: 'jmsolerl@upn.edu.co',
    colorEmail: '#969696',
    informacion: 'Información',
    informacion2: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    avatar: '/assets/images/Avatar9.png',
    seginformacion: 'Información ',
    seginformacion2: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    colorinfo:'#19A2AE'
  },
  {
    id: 5,
    personal: '/assets/images/personal5.svg',
    username: 'Diana Marcela Sánchez Yáñez',
    colorName:'#2D3248',
    email: 'dmsanchezy@pedagogica.edu.co',
    colorEmail: '#969696',
    informacion: 'Información',
    informacion2: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    avatar: '/assets/images/Avatar2.png',
    seginformacion: 'Información ',
    seginformacion2: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    colorinfo:'#19A2AE'
  },
  {
    id: 6,
    personal: '/assets/images/personal6.svg',
    username: 'Jhonny Alexander Ortegón Moreno',
    colorName:'#2D3248',
    email: 'jaortegonm@upn.edu.co',
    colorEmail: '#969696',
    informacion: 'Información',
    informacion2: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    avatar: '/assets/images/Avatar7.png',
    seginformacion: 'Información ',
    seginformacion2: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    colorinfo:'#19A2AE'
  },
  {
    id: 7,
    personal: '/assets/images/personal7.svg',
    username: 'Camila Pontón Leguizamón',
    colorName:'#2D3248',
    email: 'cpontonl@upn.edu.co',
    colorEmail: '#969696',
    informacion: 'Información',
    informacion2: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    avatar: '/assets/images/Avatar5.png',
    seginformacion: 'Información ',
    seginformacion2: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    colorinfo:'#19A2AE'
  }

];

export const App = ()=> {
  return (
    <div className="App">
       <GlobalStyles />  
      <ProfileCard  userInfo={users} />
  </div>
  )
}

