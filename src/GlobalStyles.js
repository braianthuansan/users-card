import { createGlobalStyle } from 'styled-components';


const GlobalStyle = createGlobalStyle`

/* reset */
html {
box-sizing: border-box;
font-size: 62.5%;
}
*, *::before, *::after {
  box-sizing: inherit;
}
html, body, #root, .App {
  height: 100%;
}
body {
  
  margin: 0;
  font-size: 1.6rem;
  -webkit-font-smoothing: antialiased;
  background-color: var(--color-white);
  font-family: 'Kumbh Sans', sans-serif;
}
`;

export default GlobalStyle;